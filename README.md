# EnergyProfiler

A Python package for profiling the energy consumption of code segments.

## Installation

This package is not yet available on PyPi so installation using pip+git is required

```bash
#Install matplotlib dependancy 
pip install matplotlib
#Install using pip+git
pip install git+https://gitlab.com/bburnett6/energyprofiler.git@78f636eb4196438123d7eb702a01503d5db94f46
```

## Example Usage

```python
import EnergyProfiler as ep
import time 

prof = ep.EnergyProfiler(interval=0.5)
prof.start()
time.sleep(3)
MyFirstCoolFunction()
time.sleep(3)
MySecondCoolFunction()
time.sleep(3)
prof.stop()
prof.plot('Power Usage of My Cool Functions')
```

## Known Issues

* Debian based systems have made the Intel RAPL files root only. To use this tool in userspace on Ubuntu or other Debian derivatives, the permissions on those files need to be modified.
