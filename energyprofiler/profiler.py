import csv
import time
from threading import Timer
import matplotlib.pyplot as plt 

from energyprofiler.energy_objects import RAPLEnergy, BatEnergy, GPUEnergy

#Energy profiling using a thread model based off of the code for the RepeatedTimer class here
#https://stackoverflow.com/questions/3393612/run-certain-code-every-n-seconds
class EnergyProfiler(object):
	def __init__(self, interval=0.1, output='eprofile.csv', printout=False, usebat=False):
		self.re = RAPLEnergy()
		self.be = BatEnergy()
		self.ge = GPUEnergy()
		self.interval = interval
		self.is_running = False
		self.is_print = printout
		self.usebat = usebat
		self.output = output
		self.fout = open(output, 'w') #global output file pointer. Make sure to call stop()

	def _run(self):
		self.is_running = False
		self.restart()

		#collect data
		ps = self.re.measure_power()
		if self.usebat:
			bs = self.be.measure_power()
		gs = self.ge.measure_power()
		data = [self.time_since_start()]
		if self.usebat:
			for b in bs:
				data.append(f"{b['power']}")
		for p in ps:
			data.append(f"{p['power']}")
		for g in gs:
			data.append(f'{g["power"]}')
		try:
			self.fout.write(','.join(data) + "\n")
		except:
			pass
		if self.is_print:
			print(','.join(data))

	def start(self):
		#Initialize the data output file header info
		ps = self.re.measure_power()
		if self.usebat:
			bs = self.be.measure_power()
		gs = self.ge.measure_power()
		header_info = ['timestamp']
		if self.usebat:
			for b in bs:
				header_info.append('Bat0')
		for p in ps:
			header_info.append(p['cpu'])
		for g in gs:
			header_info.append(f'gpu-{g["gpu"]}')
		self.hinfo = header_info
		self.fout.write(','.join(header_info) + "\n")
		if self.is_print:
			print(','.join(header_info))

		#Start time to measure time since start of process
		self.start_time = time.time()
		#"start" the profiler by starting the first timed process
		self.restart()

	def restart(self):
		if not self.is_running:
			self._timer = Timer(self.interval, self._run)
			self._timer.start()
			self.is_running = True

	def stop(self):
		self._timer.cancel()
		self.is_running = False
		self.fout.close()

	def time_since_start(self):
		s = time.time() - self.start_time
		return f"{s:.2f}"

	def plot(self, title='Power Plot', plot_fname='plotout.png'):
		if self.is_running:
			print("profiler is running... Stop it to plot")
			return

		toplot = []
		for h in self.hinfo:
			toplot.append({'name': h, 'data': []})
			
		with open(self.output, 'r') as f:
			for i, line in enumerate(f.readlines()):
				if i == 0: #skip header
					continue
				ls = line.split(',')
				for i, l in enumerate(ls):
					toplot[i]['data'].append(float(l))

		for i, p in enumerate(toplot):
			if i == 0:
				continue
			#print(p['data'])
			plt.plot(toplot[0]['data'], p['data'], label=p['name'])
		plt.title(title)
		plt.xlabel('Time from start of profiler (s)')
		plt.ylabel('Power Reading (W)')
		plt.legend()
		#plt.show()
		plt.savefig(plot_fname)


if __name__ == '__main__':
	mon = EnergyProfiler(printout=True)

	mon.start()
	time.sleep(10)
	mon.stop()
	mon.plot()
