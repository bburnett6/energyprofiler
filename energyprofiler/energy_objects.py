import os
import time
import sys
import re 
import subprocess as sp 

class RAPLEnergy(object):
	def __init__(self):
		base = "/sys/class/powercap"
		dirs = list(filter(lambda f: ':' in f, os.listdir(base)))

		#list of rapl file dicts. These dicts have the shape
		#{'name': name, 'path' : energy_uj}
		#The 'cpu' entry informs which cpu if there are multiple.
		#'path' is the path to the energy file
		#See https://blog.chih.me/read-cpu-power-with-RAPL.html for more details
		self.rapl_files = []

		for d in dirs:
			rapl_base = base + '/' + d
			with open(rapl_base + '/name', 'r') as f:
				name = f.read()[:-1] #don't read endline
			if name != 'core' and name != 'uncore' and name != 'dram':
				#package >= core + uncore so exclude these
				#dram is also only the in processor memory not actual system RAM
				self.rapl_files.append({'cpu': name, 'path': rapl_base + '/energy_uj'})

	def read_energy(self):
		#Read the raw energy data
		#returns a similar list of dicts to the rapl_files
		#{'type': name, 'val': e_uj}
		energies = []
		for r in self.rapl_files:
			with open(r['path'], 'r') as f:
				val = f.read()[:-1]
				energies.append({'cpu': r['cpu'], 'val': val})
		return energies

	def measure_power(self, delay=1):
		#measure the power draw over time=delay
		#when testing, the lowest I tried that worked was delay=0.001
		e1 = self.read_energy()
		time.sleep(delay)
		e2 = self.read_energy()
		measurements = []
		for ei, ej in zip(e1, e2):
			#power = E / t
			power = (abs(float(ej['val']) - float(ei['val']))) * 1e-6 / delay #uj to j
			measurements.append({'cpu': ei['cpu'], 'power': power})

		return measurements

class BatEnergy(object):
	#Help from https://unix.stackexchange.com/questions/10418/how-to-find-power-draw-in-watts
	def __init__(self):
		self.base = '/sys/class/power_supply/BAT0'

	def measure_power(self):
		with open(self.base + '/current_now', 'r') as f:
			current = float(f.read()[:-1])
		with open(self.base + '/voltage_now', 'r') as f:
			voltage = float(f.read()[:-1])
		power = (current * voltage) * 1e-12

		return [{'cpu': 'BAT0', 'power': power}]

class GPUEnergy(object):
	def __init__(self):
		#Test if there even is a gpu
		try:
			test_nvidia = "nvidia-smi > /dev/null 2>&1"
			output = sp.check_call(['bash', '-c', test_nvidia])
		except:
			self.ngpus = 0
			return

		#Get the number of gpus
		nvidia_list_gpus = "nvidia-smi -L | wc -l" #List the GPUs then pipe that into a line count
		output = sp.check_output(['bash', '-c', nvidia_list_gpus])
		self.ngpus = int(output.decode("utf-8"))

	def measure_power(self):
		measurements = []
		for i in range(self.ngpus):
			gpu_cmd = f'nvidia-smi -i {i} --format=csv,noheader --query-gpu=power.draw'
			output = sp.check_output(['bash', '-c', gpu_cmd])
			val = float(output.decode("utf-8")[:-2]) #omit the W at the end
			measurements.append({'gpu' : i, 'power': val})

		return measurements

if __name__ == '__main__':
	re = RAPLEnergy()
	ge = GPUEnergy()

	for i in range(10):
		ps = re.measure_power()
		gs = ge.measure_power()
		for p in ps:
			print(f"{p['cpu']} : {p['power']}")
		for g in gs:
			print(f"{g['gpu']} : {g['power']}")
		time.sleep(0.5)
		
