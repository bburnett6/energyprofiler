import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="energy-profiler-bburnett", # Replace with your own username
    version="0.0.14",
    author="Ben Burnett",
    author_email="bburnett@umassd.edu",
    description="A package for profiling energy usage",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bburnett6/energyprofiler",
    packages=setuptools.find_packages(),
    install_requires=[
        "matplotlib"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)